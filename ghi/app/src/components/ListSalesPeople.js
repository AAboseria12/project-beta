import React, { useState, useEffect } from "react";

function ListSalesPeople(props) {
  const [salespeople, setSalesPeople] = useState([]);

  const handleDeleteSalesPerson = async (event, id) => {
    event.preventDefault();

    const fetchOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };

    try {
      const response = await fetch(`http://localhost:8090/api/salespeople/${id}/`, fetchOptions);
      if (response.ok) {
        await loadSalesPeople();
      }
    } catch (error) {
      console.error(error);
    }
  };

  async function loadSalesPeople() {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.salespeople);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No salespeople found", e);
    }
  }

  useEffect(() => {
    loadSalesPeople();
  }, []);


  return (
    <>
      <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((salesperson) => (
            <tr key={salesperson.id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
              <td>
                <a
                  onClick={(event) => handleDeleteSalesPerson(event, salesperson.id)}
                  type="button"
                  className="btn btn-link"
                >
                  Delete
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListSalesPeople;
