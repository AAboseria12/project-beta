import React, { useState } from 'react';

function CreateCustomer() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeAddress = (event) => {
    const value = event.target.value;
    setAddress(value);
  }

  const handleChangePhoneNumber = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      address: address,
      phone_number: phoneNumber,
    };

    const customersURL = 'http://localhost:8090/api/customers/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(customersURL, fetchOptions);
      if (response.ok) {
        setFirstName('');
        setLastName('');
        setAddress('');
        setPhoneNumber('');
      }
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <>
      <h1>Add a Customer</h1>
      <form onSubmit={handleSubmit} id="add-customer-form">
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeFirstName}
            value={firstName}
            placeholder="First Name"
            required
            name="first_name"
            type="text"
            id="first_name"
            className="form-control"
          />
          <label htmlFor="first_name">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeLastName}
            value={lastName}
            placeholder="Last Name"
            required
            name="last_name"
            type="text"
            id="last_name"
            className="form-control"
          />
          <label htmlFor="last_name">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeAddress}
            value={address}
            placeholder="Address"
            required
            name="address"
            type="text"
            id="address"
            className="form-control"
          />
          <label htmlFor="address">Address</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangePhoneNumber}
            value={phoneNumber}
            placeholder="Phone Number"
            required
            name="phone_number"
            type="text"
            id="phone_number"
            className="form-control"
          />
          <label htmlFor="phone_number">Phone Number</label>
        </div>
        <button className="btn btn-primary">Add Customer</button>
      </form>
    </>
  );
}

export default CreateCustomer;
