import React, { useState, useEffect } from "react";

function ListSales(props) {
  const [sales, setSales] = useState([]);

  const handleDeleteSale = async (event, id) => {
    event.preventDefault();

    const fetchOptions = {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
    };

    try {
      const response = await fetch(`http://localhost:8090/api/sales/${id}/`, fetchOptions);
      if (response.ok) {
        await loadSales();
      }
    } catch (error) {
      console.error(error);
    }
  };

  async function loadSales() {
    try {
      const response = await fetch('http://localhost:8090/api/sales/');
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.error(response);
      }
    } catch (e) {
      console.error("No sales found", e);
    }
  }

  useEffect(() => {
    loadSales();
  }, []);

  return (
    <>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{sale.salesperson.employee_id}</td>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobile.vin}</td>
              <td>{sale.price}</td>
              <td>
                <a
                  onClick={(event) => handleDeleteSale(event, sale.id)}
                  type="button"
                  className="btn btn-link"
                >
                  Delete
                </a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default ListSales;
