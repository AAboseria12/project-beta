import React, { useEffect, useState } from 'react';

function SalesHistory() {
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [sales, setSales] = useState([]);
  const [salespeople, setSalespeople] = useState([]);

  const fetchSalespeople = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSalespersonSelection = (event) => {
    const selectedEmployeeId = event.target.value;
    setSelectedSalesperson(selectedEmployeeId);
  };

  const fetchSales = async () => {
    try {
      const response = await fetch(`http://localhost:8090/api/sales/`);
      if (response.ok) {
        const data = await response.json();
        setSales(data.sales);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchSalespeople();
    fetchSales();
  }, []);

  return (
    <>
      <h2>Sales for Selected Salesperson</h2>
      <div>
        <label>Select a Salesperson:</label>
        <select
          value={selectedSalesperson}
          onChange={handleSalespersonSelection}
        >
          <option value="">Select a salesperson...</option>
          {salespeople.map((salesperson) => (
            <option
              key={salesperson.employee_id}
              value={salesperson.employee_id}
            >
              {salesperson.first_name} {salesperson.last_name}
            </option>
          ))}
        </select>
      </div>
      {selectedSalesperson ? (
        <div className="table-container">
          <table className="table table-striped">
              <thead>
                <tr>
                  <th>Salesperson Name</th>
                  <th>Customer</th>
                  <th>VIN</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                    {sales.map((sale) => {
                      if (selectedSalesperson === sale.salesperson.employee_id) {
                        return (
                          <tr key={sale.id}>
                            <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                            <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.price}</td>
                          </tr>
                        );
                      }
                    })}
              </tbody>
          </table>
        </div>
      ) : (
        <p>Please select a salesperson.</p>
      )}
    </>
  );
}

export default SalesHistory;
