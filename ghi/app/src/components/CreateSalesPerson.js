import React, { useState } from 'react';

function CreateSalesPerson() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeId,
    };

    const salespeopleURL = 'http://localhost:8090/api/salespeople/';
    const fetchOptions = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(salespeopleURL, fetchOptions);
      if (response.ok) {
        setFirstName('');
        setLastName('');
        setEmployeeId('');
      }
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <>
      <h1>Add a Salesperson</h1>
      <form onSubmit={handleSubmit} id="add-salesperson-form">
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeFirstName}
            value={firstName}
            placeholder="First Name"
            required
            name="first_name"
            type="text"
            id="first_name"
            className="form-control"
          />
          <label htmlFor="first_name">First Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeLastName}
            value={lastName}
            placeholder="Last Name"
            required
            name="last_name"
            type="text"
            id="last_name"
            className="form-control"
          />
          <label htmlFor="last_name">Last Name</label>
        </div>
        <div className="form-floating mb-3">
          <input
            onChange={handleChangeEmployeeId}
            value={employeeId}
            placeholder="Employee ID"
            required
            name="employee_id"
            type="text"
            id="employee_id"
            className="form-control"
          />
          <label htmlFor="employee_id">Employee ID</label>
        </div>
        <button className="btn btn-primary">Add Salesperson</button>
      </form>
    </>
  );
}

export default CreateSalesPerson;
