import React, { useState, useEffect } from 'react';

function RecordSales() {
  const [automobiles, setAutomobiles] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [selectedAutomobile, setSelectedAutomobile] = useState('');
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [selectedCustomer, setSelectedCustomer] = useState('');
  const [price, setPrice] = useState('');

  const fetchAutomobiles = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const fetchSalespeople = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const fetchCustomers = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const salesRecord = {
      automobile: selectedAutomobile,
      salesperson: selectedSalesperson,
      customer: selectedCustomer,
      price: parseFloat(price),
    };

    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(salesRecord),
      });

      if (response.ok) {
        setSelectedAutomobile('');
        setSelectedSalesperson('');
        setSelectedCustomer('');
        setPrice('');
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchAutomobiles();
    fetchSalespeople();
    fetchCustomers();
  }, []);

  return (
    <>
      <h1>Record a new sale</h1>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="selectedAutomobile" className="form-label">Automobile VIN</label>
          <select
            value={selectedAutomobile}
            onChange={(event) => setSelectedAutomobile(event.target.value)}
            className="form-control"
            id="selectedAutomobile"
          >
            <option value="">Choose an automobile VIN</option>
            {automobiles.map((automobile) => {
              if (!automobile.sold) {
                return (
                  <option key={automobile.vin} value={automobile.vin}>
                    {automobile.vin}
                  </option>
                );
              }
            })}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="selectedSalesperson" className="form-label">Salesperson</label>
          <select
            value={selectedSalesperson}
            onChange={(event) => setSelectedSalesperson(event.target.value)}
            className="form-control"
            id="selectedSalesperson"
          >
            <option value="">Choose a salesperson</option>
            {salespeople.map((salesperson) => (
              <option key={salesperson.employee_id} value={salesperson.employee_id}>
                {salesperson.first_name} {salesperson.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="selectedCustomer" className="form-label">Customer</label>
          <select
            value={selectedCustomer}
            onChange={(event) => setSelectedCustomer(event.target.value)}
            className="form-control"
            id="selectedCustomer"
          >
            <option value="">Choose a customer</option>
            {customers.map((customer) => (
              <option key={customer.id} value={customer.id}>
                {customer.first_name} {customer.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="price" className="form-label">Price</label>
          <input
            type="number"
            value={price}
            onChange={(event) => setPrice(event.target.value)}
            className="form-control"
            id="price"
          />
        </div>
        <button type="submit" className="btn btn-primary">Record Sale</button>
      </form>
    </>
  );
}

export default RecordSales;
