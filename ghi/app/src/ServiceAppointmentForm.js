import React, { useEffect, useState } from "react";


function ServiceAppointment() {
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [reason, setReason] = useState('');
    const [time, setTime] = useState('');

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.vin = vin;
        data.customer = customer;
        data.date_time = new Date(date).toISOString().slice(0, 10);
        data.technician = technician;
        data.reason = reason;
        data.time = time;

        const apptUrl = "http://localhost:8080/api/appointments/";
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json',
            },
        };
        const response = await fetch(apptUrl, fetchOptions);
        if (response.ok) {
            const newAppointment = await response.json();


            setTechnician('');
            setVin('');
            setCustomer('');
            setDate('');
            setReason('');
            setTime('');
        }
    };


    const handleVinchange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerchange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDatechange = (event) => {
        const value = event.target.value;
        setDate(value);
    }


    const handleReasonchange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleTechnicianchange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    const handleTimechange = (event) => {
        const value = event.target.value;
        setTime(value);
    }



    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="off-set-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a service appointment</h1>
                    <form onSubmit={handleSubmit} id="create-an-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={vin} onChange={handleVinchange} placeholder="vin" required type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customer} onChange={handleCustomerchange} placeholder="customer" required type="text" id="customer" name="customer" className="form-control" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={date} onChange={handleDatechange} placeholder="date" required type="date" id="date" name="date_time" className="form-control" />
                            <label htmlFor="date_time">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={time} onChange={handleTimechange} placeholder="time" required type="time" id="time" name="time" className="form-control" />
                            <label htmlFor="time">Time</label>
                        </div>

                        <div className="mb-3">
                            <select value={technician} onChange={handleTechnicianchange} placeholder="technician" required id="technician" name="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={reason} onChange={handleReasonchange} placeholder="reason" required type="text" id="reason" name="reason" className="form-control" />
                            <label htmlFor="time">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div >
        </div >
    )
};
export default ServiceAppointment;
