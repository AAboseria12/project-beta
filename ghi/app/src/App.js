import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSalesPeople from './components/ListSalesPeople';
import ListCustomers from './components/ListCustomers';
import ListSales from './components/ListSales';
import ListVehicles from './components/ListVehicle';
import ListAutomobiles from './components/ListAutos';
import CreateVehicleModel from './components/CreateVehicle';
import CreateSalesPerson from './components/CreateSalesPerson';
import CreateCustomer from './components/CreateCustomer';
import RecordSale from './components/CreateSale';
import SalesHistory from './components/SalesHistory';
import AddTech from './AddTech';
import TechnicianList from './ListTech';
import ServiceAppointment from './ServiceAppointmentForm';
import ApptsList from './ListAppt';
import ServiceHistory from './ServiceHistory';
import Listmanufacturer from './Listmanufacturer';
import AddManufacturer from './AddManufacturer';
import AddAuto from './AddAuto';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople" element={<ListSalesPeople />} />
          <Route path="/customers" element={<ListCustomers />} />
          <Route path="/sales" element={<ListSales />} />
          <Route path="/models" element={<ListVehicles />} />
          <Route path="/automobiles" element={<ListAutomobiles />} />
          <Route path="/models/create" element={<CreateVehicleModel />} />
          <Route path="/salespeople/create" element={<CreateSalesPerson />} />
          <Route path="/customers/create" element={<CreateCustomer />} />
          <Route path="/sales/create" element={<RecordSale />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path='/technicians/create' element={<AddTech />} />
          <Route path='/technicians' element={<TechnicianList />} />
          <Route path='/appointments/create' element={<ServiceAppointment />} />
          <Route path='/appointments' element={<ApptsList />} />
          <Route path='/appointments/history' element={<ServiceHistory />} />
          <Route path='/manufacturers' element={<Listmanufacturer />} />
          <Route path='/manufacturers/create' element={<AddManufacturer />} />
          <Route path='/automobiles/create' element={<AddAuto />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
