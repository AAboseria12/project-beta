from django.urls import path
from .views import api_list_Salespeople, api_list_Customers, api_list_Sales

urlpatterns = [
    path("salespeople/", api_list_Salespeople, name="api_list_Salespeople"),
    path("salespeople/<int:id>/", api_list_Salespeople, name="api_list_Salespeople"),
    path("customers/", api_list_Customers, name="api_list_Customers"),
    path("customers/<int:id>/", api_list_Customers, name="api_list_Customers"),
    path("sales/", api_list_Sales, name="api_list_Sales"),
    path("sales/<int:id>/", api_list_Sales, name="api_list_Sales"),
]
