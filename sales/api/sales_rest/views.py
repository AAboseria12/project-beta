from django.http import JsonResponse,Http404
from django.shortcuts import get_object_or_404
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
import json
from .encoders import CustomerListEncoder, SaleListEncoder, SalesPersonListEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO

# Create your views here.


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_Salespeople(request, id=None):
    if request.method == "GET":
        salesPeople = Salesperson.objects.all()
        if not salesPeople:
            return JsonResponse(
                {"message": "No Salespeople found"},
                status=404,
            )
        return JsonResponse(
            {"salespeople": salesPeople},
            encoder= SalesPersonListEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            salesPerson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesPerson,
                encoder= SalesPersonListEncoder,
                safe=False,
            )
        except IntegrityError:
            response = JsonResponse(
                {"message": "Not unique employee_id"}
            )
            response.status_code = 400
            return response
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON for Salesperson"},
                status=400,
            )
    else:
        if id is not None:
            try:
                salesPerson = get_object_or_404(Salesperson, id=id)
                salesPerson.delete()
                return JsonResponse({"message": "salesperson deleted successfully"})
            except Http404:
                return JsonResponse(
                    {"message": "salesperson not found"},
                    status=404
                )
        else:
            return JsonResponse(
                {"message": "Missing 'id' parameter"},
                status=400,
            )


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_Customers(request, id=None):
    if request.method == "GET":
        customers = Customer.objects.all()
        if not customers:
            return JsonResponse(
                {"message": "No Customers found"},
                status=404,
            )
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerListEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except IntegrityError:
            response = JsonResponse(
                {"message": "Not unique phone_number"}
            )
            response.status_code = 400
            return response
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON for Customer"},
                status=400,
            )
    else:
        if id is not None:
            try:
                customer = get_object_or_404(Customer, id=id)
                customer.delete()
                return JsonResponse({"message": "Customer deleted successfully"})
            except Http404:
                return JsonResponse(
                    {"message": "Customer not found"},
                    status=404
                )
        else:
            return JsonResponse(
                {"message": "Missing 'id' parameter"},
                status=400,
            )


@require_http_methods(["GET", "POST", "DELETE"])
def api_list_Sales(request, id=None):
    if request.method == "GET":
        sales = Sale.objects.all()
        if not sales:
            return JsonResponse(
                {"message": "No Sales found"},
                status=404,
            )
        return JsonResponse(
            {"sales": sales},
            encoder=SaleListEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON for Sale"},
                status=400,
            )
        try:
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
        except:
            return JsonResponse(
                {"message": "Invalid employee id"},
                status=400,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=400,
            )
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except:
            return JsonResponse(
                {"message": "Invalid VIN"},
                status=400,
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder = SaleListEncoder,
            safe=False,
        )
    else:
        if id is not None:
            try:
                sale = get_object_or_404(Sale, id=id)
                sale.delete()
                return JsonResponse({"message": "sale deleted successfully"})
            except Http404:
                return JsonResponse(
                    {"message": "sales not found"},
                    status=404
                )
        else:
            return JsonResponse(
                {"message": "Missing 'id' parameter"},
                status=400,
            )
