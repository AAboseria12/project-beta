# CarCar

Team:

* Anastasia Dwarica-Pham - service
* Abdullah Aboseria - Sales

## Design

## Service microservice

AutomobileVO Model: This model represents an automobile identified by its VIN. This model allows you to store information about individual automobiles.
Technician Model: This model represents a technician who can perform service appointments.This model allows you to store information about service technicians.
Appointment Model: This model represents a service appointment scheduled with a technician. This model allows you to schedule and track service appointments, associate them with automobiles, and assign technicians.
To integrate these models with an inventory microservice,  an API endpoints were established between the two services.

## Sales microservice

First I will add the app in the installed apps for my sales project settings. Next I create the three models, sales-salesperson-customer and AutomobileVO model. I will migrate the changes, then I will create the poller and test in docker to make sure it is working. After that I will create the RESTFUL API endpoints and register the URLS with Insomina to make sure it is working. Lastly, I wil work on the react components so the front-end is fully functioning.
